package com.example.mysql.repository;
import org.springframework.data.repository.CrudRepository;
import com.example.mysql.entity.User;
public interface UserRepository extends CrudRepository<User, Integer> {

}